package bd.mapsidejoin;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class MapSideJoinApp extends Configured implements Tool {


    public static void main(String[] args) throws Exception {
        int result = ToolRunner.run(new Configuration(), new MapSideJoinApp(), args);
        System.exit(result);
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration configuration = getConf();
        configuration.set("mapreduce.output.textoutputformat.separator", ",");

        Job job = Job.getInstance(configuration, "map side join app");

        Path primaryInput = new Path(args[0]);
        Path cacheInput = new Path(args[1]);
        Path output = new Path(args[2]);

        FileInputFormat.addInputPath(job, primaryInput);
        FileOutputFormat.setOutputPath(job, output);
        job.addCacheFile(cacheInput.toUri());

        job.setMapperClass(MapSideJoinMapper.class);
        job.setMapOutputKeyClass(JoinKey.class);
        job.setMapOutputValueClass(DoubleWritable.class);


        job.setReducerClass(MapSideJoinReducer.class);
        job.setOutputKeyClass(JoinKey.class);
        job.setOutputValueClass(DoubleWritable.class);

        job.setNumReduceTasks(1);

        FileSystem fs = FileSystem.get(configuration);
        if (fs.exists(output)) {
            fs.delete(output, true);
        }

        return job.waitForCompletion(true) ? 0 : 1;

    }
}
