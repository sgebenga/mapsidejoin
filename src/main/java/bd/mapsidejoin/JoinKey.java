package bd.mapsidejoin;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class JoinKey implements WritableComparable<JoinKey> {

    private int id;
    private String name;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void write(DataOutput out) throws IOException {
        out.writeInt(id);
        out.writeUTF(name);
    }

    public void readFields(DataInput in) throws IOException {
        id = in.readInt();
        name = in.readUTF();
    }

    @Override
    public int compareTo(JoinKey o) {
        int result = Integer.valueOf(id).compareTo(Integer.valueOf(o.id));
        if (result == 0) {
            // result = name.compareTo(o.name);
        }
        return result;
    }

    @Override
    public String toString() {
        return id + "," + name;
    }
}
