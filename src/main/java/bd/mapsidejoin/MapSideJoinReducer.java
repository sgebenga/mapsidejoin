package bd.mapsidejoin;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class MapSideJoinReducer extends Reducer<JoinKey, DoubleWritable, JoinKey, DoubleWritable> {

    @Override
    protected void reduce(JoinKey key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
        double acum = 0;
        for (DoubleWritable value : values) {
            acum += value.get();
        }
        context.write(key, new DoubleWritable(acum));
    }
}
