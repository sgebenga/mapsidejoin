package bd.mapsidejoin;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class MapSideJoinMapper extends Mapper<LongWritable, Text, JoinKey, DoubleWritable> {

    private Map<Integer, JoinKey> cache = new HashMap<Integer, JoinKey>();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        URI[] cacheFiles = context.getCacheFiles();
        System.out.println(cacheFiles.length);
        if (cacheFiles.length > 0) {
            Path path = new Path(cacheFiles[0]);
            FileSystem fs = path.getFileSystem(context.getConfiguration());
            FileStatus[] statuses = fs.listStatus(path);
            for (FileStatus status : statuses) {
                loadCustomer(fs, status, context);
            }
        }
    }

    private void loadCustomer(FileSystem fs, FileStatus status, Context context) throws IOException {
        try (BufferedReader reader = new BufferedReader((new InputStreamReader(fs.open(status.getPath()))))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] words = StringUtils.split(line, StringUtils.COMMA);
                JoinKey key = new JoinKey();
                key.setId(Integer.parseInt(words[0]));
                key.setName(words[1]);
                cache.put(key.getId(), key);
            }
        }
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] words = StringUtils.split(value.toString(), StringUtils.COMMA);
        int id = Integer.parseInt(words[0]);
        JoinKey joinKey = cache.get(id);
        if (joinKey != null) {
            context.write(joinKey, new DoubleWritable(Double.parseDouble(words[2])));
        }
    }
}
